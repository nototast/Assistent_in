# Assistent*in für geschlechtergerechte/inklusive Sprache

## Background
##### general:
- [Wikipedia: Gender Neutrality in languages with Grammatical Gender: German](https://en.wikipedia.org/wiki/Gender_neutrality_in_languages_with_grammatical_gender#German)
- [Wikipedia: Existing Laws and Regulations in DACH](https://de.wikipedia.org/wiki/Gesetze_und_amtliche_Regelungen_zur_geschlechtergerechten_Sprache#cite_note-2)
- [Wikipedia: Gender Neutral Language](https://de.wikipedia.org/wiki/Geschlechtergerechte_Sprache)

##### Journalism:
- [tagesspiegel announces their gender inclusive guidelines](https://www.tagesspiegel.de/politik/in-eigener-sache-die-tagesspiegel-redaktion-gibt-sich-leitlinien-fuer-geschlechtergerechte-sprache/26834766.html)
- [tagesspiegel announcement follow up](https://www.tagesspiegel.de/politik/geschlechtergerechte-sprache-im-tagesspiegel-was-sie-davon-halten-dass-wir-jetzt-gendern/26894858.html)
- [Zeit podcast discussing their approach](https://www.zeit.de/politik/2021-02/gendergerechte-sprache-gendern-politikpodcast)
- [Netzpolitik's appriach to Gender Neutrality](https://netzpolitik.org/2020/warum-wir-geschlechtergerechte-sprache-verwenden/)

##### Public:
- [Hannover's rules for inclusive language](https://www.hannover.de/Leben-in-der-Region-Hannover/Verwaltungen-Kommunen/Die-Verwaltung-der-Landeshauptstadt-Hannover/Gleichstellungsbeauf%C2%ADtragte-der-Landeshauptstadt-Hannover/Aktuelles/Neue-Regelung-f%C3%BCr-geschlechtergerechte-Sprache)
- [Swiss Government's Editing Support Material for Gender Neutral Gemran](https://www.bk.admin.ch/bk/de/home/dokumentation/sprachen/hilfsmittel-textredaktion/leitfaden-zum-geschlechtergerechten-formulieren.html)
- [Charité's guide for gender neutral language](https://frauenbeauftragte.charite.de/metas/meldung/artikel/detail/geschlechtergerechte_sprache_an_der_charite/)
- [Ein Project von Jounalisitinnen Bund. "... nützliche Tipps & Tools, wie Sie diskriminierungsfrei schreiben und sprechen können"](https://www.genderleicht.de/)
- [Duden's handbook for gender neutral language](https://shop.duden.de/products/handbuch-geschlechtergerechte-sprache)
- [Duden's guide for writing gender correctly](https://shop.duden.de/products/richtig-gendern)\
I own the last two, and together with the Swiss Govemerment PDF - we already have a lot to work with for establiching our approach to detecting Gender.

##### Council for German:
- [Last update](http://www.rechtschreibrat.com/DOX/rfdr_2018-11-28_anlage_3_bericht_ag_geschlechterger_schreibung.pdf)
- [Recommendations](http://www.rechtschreibrat.com/DOX/rfdr_PM_2018-11-16_Geschlechtergerechte_Schreibung.pdf)
- [Early discussion](http://www.rechtschreibrat.com/DOX/rfdr_PM_2018-06-08_Geschlechtergerechte_Schreibung.pdf)


##### Relevant Research and Researchers:
- [Prof. Dr. Gabriele Diewald](https://www.gabrielediewald.de/forschung.html)

##### Related tools:
Similar writing support tools without a gender inclusive feature set. The folowing are all commercial and closed tools.
- [Duden Mentor, German only](https://www.duden.de/rechtschreibpruefung-online)
- [LanguageTool, also German](https://languagetool.org/)
- [GermanCorrector](https://www.germancorrector.com/)
- [Grammerly, English Only](https://app.grammarly.com/)

Open Source pronoun highlighter for English
- Pronoun Highlighter [Repository](https://github.com/bencgreenberg/pronoun_highlighter) and [Web-App](https://gender-pronoun-highlighter.herokuapp.com/).
This is a good example for simple first approach, e.g. it searches and highlights she, or herself, but not chairwoman

##### Related work to promote inclusivity and representation:
- [Duden adds female representation in professions](https://www.zeit.de/kultur/2021-01/gendergerechte-sprache-duden-kathrin-kunkel-razum)
- [Representation in traffic signs](https://www.zeit.de/hamburg/2021-02/verkehrsschilder-piktogramm-gender-vielfalt-innovation-spd-peter-ernst-schuett)
