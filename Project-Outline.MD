# Milestones

1. Web-App with basic pipeline for pronoun detection
    1. use spacy for the base model
    2. use [Gradio](https://gradio.app/) for prototyping quickly
    3. When happy build the full stack website using fastAPI
2. Write and prioritise our list of gender detection "rules"
    1. Use swiss source, and my german grammar books ;), to clearly write what cases of gender detection we could address
    2. Sketch a first approach to use spacy to detect and clarify where we expect to encounter difficulties 
    3. cluster which rules it makes sense to work on together
    4. prioritise list for next development sprints
3. Write up the documentation section clarifying what do we mean by detecting gender relating to people in a way that den help writers and editors
    1. This will be based on the rules and explain what complexities we are aware of, or when inaccuracies could occur so that our users know how to use the tool
    2. Establish a clear system of highlights or annotation to communicate what we identified, e.g. information about why the word was detected, consistency warning where we detect a word that refers to a person previously mentioned and requires reading previous text to make a decision etc.
4. work through the list to develop the detection methods in the defined sprints
    1. develop and test rules
    2. develop highlight system 
    3. analyse detection accuracy and assign "warning" annotation where we encounter uncertainty
    4. deploy new rules and update the documentation to clarify current state of the art
    5. add documentation (maybe blog?) about what our analysis reveals
5. Marketing!
    1. Write talk for CCC and other technical conferences
    2. Write talk for non-technical conferences
    3. Write press-release
    4. Gather list of media outlets and podcasts to send our press release to 

Given sufficient time (depends on how long step 4's iterations end up taking) we will add the following before step 5. Otherwise we will do it later on (beyond the prototype scope):  
6. User Research and UX Design:
Use usability interviews with a few journalists, HR, and PR, and/or survey to inform improvements to the website UX 

Important to mention:
- We need to add a clarification that people should not submit Personally Identifying Information as our prototype won't meet the required GDPR legal requirement/
- We will use [Wave](https://wave.webaim.org/) from early development of the website to ensure the prototype is as accessible as possible and we will document clearly what we cannot address during the prototype so it could be improved later on
